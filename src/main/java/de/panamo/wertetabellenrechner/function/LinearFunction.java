package de.panamo.wertetabellenrechner.function;


import java.util.List;

public class LinearFunction implements MathFunction {
    private double m, b;

    @Override
    public double invoke(double x) {
        return (this.m * x) + this.b;
    }

    @Override
    public void insertVariables(List<Double> variables) {
        this.m = variables.get(0);
        this.b = variables.get(1);
    }
}
