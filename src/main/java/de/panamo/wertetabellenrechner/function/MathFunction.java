package de.panamo.wertetabellenrechner.function;


import java.util.List;

public interface MathFunction {

    double invoke(double x);
    void insertVariables(List<Double> variables);

}
