package de.panamo.wertetabellenrechner.setup.stage;


import de.panamo.wertetabellenrechner.setup.SetupStage;

import java.util.ArrayList;
import java.util.List;

public class TableConditionsStage extends SetupStage<List<Double>> {


    public TableConditionsStage() {
        super("Gebe an, welchen Rahmen die x-Werte der Tabelle haben sollen.", "Zwei Zahlen durch ein Leerzeichen getrennt (z.B. -7 7)");
    }

    @Override
    public List<Double> handleInput(String input) {
        String[] rawNumbers = input.split(" ");
        if(rawNumbers.length == 2) {
            try {
                List<Double> vars = new ArrayList<>();
                double var1 = Double.valueOf(rawNumbers[0]);
                double var2 = Double.valueOf(rawNumbers[1]);

                if(var1 >= var2) {
                    System.out.println("Die zweite Zahl muss größer als die Erste sein.");
                    return null;
                }

                vars.add(var1);
                vars.add(var2);
                return vars;
            } catch (NumberFormatException exception) {
                System.out.println("Bitte gebe zwei Zahlen an");
            }
        } else
            System.out.println("Bitte gebe zwei Zahlen an");
        return null;
    }
}
