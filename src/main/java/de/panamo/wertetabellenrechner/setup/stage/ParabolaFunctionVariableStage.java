package de.panamo.wertetabellenrechner.setup.stage;


import de.panamo.wertetabellenrechner.setup.SetupStage;

import java.util.ArrayList;
import java.util.List;

public class ParabolaFunctionVariableStage extends SetupStage<List<Double>> {


    public ParabolaFunctionVariableStage() {
        super("Gebe die Parameter der Parabel a, b und c an.", "Drei Zahlen durch ein Leerzeichen getrennt (z.B. -2 3 -5)");
    }

    @Override
    public List<Double> handleInput(String input) {
        String[] rawNumbers = input.split(" ");
        if(rawNumbers.length == 3) {
            try {
                List<Double> vars = new ArrayList<>();
                vars.add(Double.valueOf(rawNumbers[0]));
                vars.add(Double.valueOf(rawNumbers[1]));
                vars.add(Double.valueOf(rawNumbers[2]));
                return vars;
            } catch (NumberFormatException exception) {
                System.out.println("Bitte gebe drei Zahlen an");
            }
        } else
            System.out.println("Bitte gebe drei Zahlen an");
        return null;
    }
}
