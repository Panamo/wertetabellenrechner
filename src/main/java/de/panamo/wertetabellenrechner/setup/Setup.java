package de.panamo.wertetabellenrechner.setup;



import de.panamo.wertetabellenrechner.function.LinearFunction;
import de.panamo.wertetabellenrechner.function.MathFunction;
import de.panamo.wertetabellenrechner.setup.stage.*;

import java.util.*;

public class Setup {
    private Scanner scanner = new Scanner(System.in);

    private MathFunction function;
    private List<Double> functionVariables;
    private List<Double> tableConditions;
    private Double tableDistance;

    public Setup() {
        this.function = this.runStage(new FunctionChooseStage());
        this.functionVariables = this.runStage(this.function instanceof LinearFunction ? new LinearFunctionVariableStage() : new ParabolaFunctionVariableStage());
        this.tableConditions = this.runStage(new TableConditionsStage());
        this.tableDistance = this.runStage(new TableDistanceStage());

        this.calculateTable();
    }

    private <ResultType> ResultType runStage(SetupStage<ResultType> stage) {
        System.out.println(stage.getMessage() + " [" + stage.getInputTypeDescriptor() + "]");
        ResultType result = stage.handleInput(this.scanner.nextLine());
        if(result == null)
            return this.runStage(stage);
        return result;
    }

    private void calculateTable() {
        this.function.insertVariables(this.functionVariables);

        double tableStart = this.tableConditions.get(0);
        double tableEnd = this.tableConditions.get(1);

        System.out.println("x    |    y");
        System.out.println("-----------");

        for(double x = tableStart; x < tableEnd + tableDistance; x += tableDistance)
            System.out.println(x + "    " + this.function.invoke(x));
    }


}
